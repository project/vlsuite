<?php

namespace Drupal\Tests\vlsuite_block\Functional;

use Drupal\Tests\vlsuite\Functional\VlsuiteAdminTestBase;

/**
 * VLSuite block admin tests.
 *
 * @group vlsuite
 * @group vlsuite_block
 */
class VlsuiteBlockAdminTest extends VlsuiteAdminTestBase {

  /**
   * {@inheritdoc}
   */
  protected $vlsuiteAdminSubPath = '/block';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'vlsuite_block',
  ];

}
