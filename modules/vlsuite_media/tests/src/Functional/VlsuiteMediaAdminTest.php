<?php

namespace Drupal\Tests\vlsuite_media\Functional;

use Drupal\Tests\vlsuite\Functional\VlsuiteAdminTestBase;

/**
 * VLSuite media admin tests.
 *
 * @group vlsuite
 * @group vlsuite_media
 */
class VlsuiteMediaAdminTest extends VlsuiteAdminTestBase {

  /**
   * {@inheritdoc}
   */
  protected $vlsuiteAdminSubPath = '/media';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'vlsuite_media',
  ];

}
