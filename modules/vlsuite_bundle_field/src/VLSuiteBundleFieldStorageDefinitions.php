<?php

namespace Drupal\vlsuite_bundle_field;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\vlsuite_bundle_field\BundleField\VLSuiteBundleFieldAttachmentsTrait;
use Drupal\vlsuite_bundle_field\BundleField\VLSuiteBundleFieldBackgroundTrait;
use Drupal\vlsuite_bundle_field\BundleField\VLSuiteBundleFieldCtaTrait;
use Drupal\vlsuite_bundle_field\BundleField\VLSuiteBundleFieldDateTrait;
use Drupal\vlsuite_bundle_field\BundleField\VLSuiteBundleFieldIconTrait;
use Drupal\vlsuite_bundle_field\BundleField\VLSuiteBundleFieldIconFontIconTrait;
use Drupal\vlsuite_bundle_field\BundleField\VLSuiteBundleFieldImageTrait;
use Drupal\vlsuite_bundle_field\BundleField\VLSuiteBundleFieldLocalVideoTrait;
use Drupal\vlsuite_bundle_field\BundleField\VLSuiteBundleFieldMediaTrait;
use Drupal\vlsuite_bundle_field\BundleField\VLSuiteBundleFieldMediasTrait;
use Drupal\vlsuite_bundle_field\BundleField\VLSuiteBundleFieldRemoteVideoTrait;
use Drupal\vlsuite_bundle_field\BundleField\VLSuiteBundleFieldStringTrait;
use Drupal\vlsuite_bundle_field\BundleField\VLSuiteBundleFieldTextTrait;

/**
 * VLSuite bundle field storage definitions.
 *
 * @see vlsuite_bundle_field_entity_field_storage_info().
 */
class VLSuiteBundleFieldStorageDefinitions {

  use VLSuiteBundleFieldAttachmentsTrait {
    VLSuiteBundleFieldAttachmentsTrait::bundleFieldDefinitions as bundleFieldDefinitionsAttachments;
  }

  use VLSuiteBundleFieldBackgroundTrait {
    VLSuiteBundleFieldBackgroundTrait::bundleFieldDefinitions as bundleFieldDefinitionsBackground;
  }

  use VLSuiteBundleFieldCtaTrait {
    VLSuiteBundleFieldCtaTrait::bundleFieldDefinitions as bundleFieldDefinitionsCta;
  }

  use VLSuiteBundleFieldDateTrait {
    VLSuiteBundleFieldDateTrait::bundleFieldDefinitions as bundleFieldDefinitionsDate;
  }

  use VLSuiteBundleFieldIconTrait {
    VLSuiteBundleFieldIconTrait::bundleFieldDefinitions as bundleFieldDefinitionsIcon;
  }

  use VLSuiteBundleFieldIconFontIconTrait {
    VLSuiteBundleFieldIconFontIconTrait::bundleFieldDefinitions as bundleFieldDefinitionsIconFontIcon;
  }


  use VLSuiteBundleFieldImageTrait {
    VLSuiteBundleFieldImageTrait::bundleFieldDefinitions as bundleFieldDefinitionsImage;
  }


  use VLSuiteBundleFieldLocalVideoTrait {
    VLSuiteBundleFieldLocalVideoTrait::bundleFieldDefinitions as bundleFieldDefinitionsLocalVideo;
  }


  use VLSuiteBundleFieldMediaTrait {
    VLSuiteBundleFieldMediaTrait::bundleFieldDefinitions as bundleFieldDefinitionsMedia;
  }


  use VLSuiteBundleFieldMediasTrait {
    VLSuiteBundleFieldMediasTrait::bundleFieldDefinitions as bundleFieldDefinitionsMedias;
  }

  use VLSuiteBundleFieldRemoteVideoTrait {
    VLSuiteBundleFieldRemoteVideoTrait::bundleFieldDefinitions as bundleFieldDefinitionsRemoteVideo;
  }


  use VLSuiteBundleFieldStringTrait {
    VLSuiteBundleFieldStringTrait::bundleFieldDefinitions as bundleFieldDefinitionsString;
  }

  use VLSuiteBundleFieldTextTrait {
    VLSuiteBundleFieldTextTrait::bundleFieldDefinitions as bundleFieldDefinitionsText;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {
    $fields = [];

    $fields += self::bundleFieldDefinitionsAttachments($entity_type, $bundle, $base_field_definitions);
    $fields += self::bundleFieldDefinitionsBackground($entity_type, $bundle, $base_field_definitions);
    $fields += self::bundleFieldDefinitionsCta($entity_type, $bundle, $base_field_definitions);
    $fields += self::bundleFieldDefinitionsDate($entity_type, $bundle, $base_field_definitions);
    $fields += self::bundleFieldDefinitionsIcon($entity_type, $bundle, $base_field_definitions);
    $fields += self::bundleFieldDefinitionsIconFontIcon($entity_type, $bundle, $base_field_definitions);
    $fields += self::bundleFieldDefinitionsImage($entity_type, $bundle, $base_field_definitions);
    $fields += self::bundleFieldDefinitionsLocalVideo($entity_type, $bundle, $base_field_definitions);
    $fields += self::bundleFieldDefinitionsMedia($entity_type, $bundle, $base_field_definitions);
    $fields += self::bundleFieldDefinitionsMedias($entity_type, $bundle, $base_field_definitions);
    $fields += self::bundleFieldDefinitionsRemoteVideo($entity_type, $bundle, $base_field_definitions);
    $fields += self::bundleFieldDefinitionsString($entity_type, $bundle, $base_field_definitions);
    // Text & text extra using same declaration as original.
    $fields += self::bundleFieldDefinitionsText($entity_type, $bundle, $base_field_definitions);
    $fields['vlsuite_text_extra'] = clone $fields['vlsuite_text'];
    return $fields;
  }

}
