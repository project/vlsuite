# VLSuite Bundle Fields

Provides the following set of Drupal fields ready to use on your project.

- Image
- Icon
- Local video
- Remote video
- CTA
- Text

These fields are commonly implemented on every project, and they can be reused across any entity on your project.

Some advantages on reusing fields are:

**Development efficiency:** There is no need to create new fields each time a similar type of content is required. This saves time and effort during the development process, as you can take advantage of existing fields and their settings, avoiding unnecessary duplication.

**Data-structure consistency:** This is especially beneficial when you have multiple content types that share similar characteristics. Reusable fields allow you to establish a consistent and standardized structure, which makes it easier to manage and understand the data on the site.

**Customizable settings per entity:** If needed, you will be able to customize the field configuration depending on the entity. Let's say there are Landing and Article content types on your project: both contents can reuse the Image field, but having a different Image style defined on their display.
