<?php

namespace Drupal\Tests\vlsuite_modal\Functional;

use Drupal\Tests\vlsuite\Functional\VlsuiteAdminTestBase;

/**
 * VLSuite modal admin tests.
 *
 * @group vlsuite
 * @group vlsuite_modal
 */
class VlsuiteModalAdminTest extends VlsuiteAdminTestBase {

  /**
   * {@inheritdoc}
   */
  protected $vlsuiteAdminSubPath = '/modal';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'vlsuite_modal',
  ];
}
