<?php

namespace Drupal\Tests\vlsuite_animations\Functional;

use Drupal\Tests\vlsuite\Functional\VlsuiteAdminTestBase;

/**
 * VLSuite animations admin tests.
 *
 * @group vlsuite
 * @group vlsuite_animations
 */
class VlsuiteAnimationsAdminTest extends VlsuiteAdminTestBase {

  /**
   * {@inheritdoc}
   */
  protected $vlsuiteAdminSubPath = '/animations';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'vlsuite_animations',
  ];
}
