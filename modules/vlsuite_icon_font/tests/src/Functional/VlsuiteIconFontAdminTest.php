<?php

namespace Drupal\Tests\vlsuite_icon_font\Functional;

use Drupal\Tests\vlsuite\Functional\VlsuiteAdminTestBase;

/**
 * VLSuite icon font admin tests.
 *
 * @group vlsuite
 * @group vlsuite_icon_font
 */
class VlsuiteIconFontAdminTest extends VlsuiteAdminTestBase {

  /**
   * {@inheritdoc}
   */
  protected $vlsuiteAdminSubPath = '/icon-font';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'vlsuite_icon_font',
  ];

}
