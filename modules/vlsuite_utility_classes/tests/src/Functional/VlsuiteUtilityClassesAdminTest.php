<?php

namespace Drupal\Tests\vlsuite_utility_classes\Functional;

use Drupal\Tests\vlsuite\Functional\VlsuiteAdminTestBase;

/**
 * VLSuite utility classes admin tests.
 *
 * @group vlsuite
 * @group vlsuite_utility_classes
 */
class VlsuiteUtilityClassesAdminTest extends VlsuiteAdminTestBase {

  /**
   * {@inheritdoc}
   */
  protected $vlsuiteAdminSubPath = '/utility-classes';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'vlsuite_utility_classes',
  ];
}
