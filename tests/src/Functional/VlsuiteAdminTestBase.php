<?php

namespace Drupal\Tests\vlsuite\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * VLSuite admin tests.
 *
 * @group vlsuite
 */
abstract class VlsuiteAdminTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * VLSuite admin permission.
   *
   * @var string
   */
  protected $vlsuiteAdminPermission = 'administer vlsuite settings';

  /**
   * VLSuite admin path.
   *
   * @var string
   */
  protected $vlsuiteAdminPath = 'admin/config/vlsuite';

  /**
   * VLSuite admin sub-path.
   *
   * @var string|NULL
   */
  protected $vlsuiteAdminSubPath = NULL;

  /**
   * A user with the 'Administer Visual Layout Suite (VLSuite) settings' perm.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'vlsuite',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser([$this->vlsuiteAdminPermission]);
  }

  /**
   * Tests admin path response code.
   */
  public function testAdminPath() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet($this->vlsuiteAdminPath . $this->vlsuiteAdminSubPath ?? '');
    $this->assertSession()->statusCodeEquals(200);
  }

}
