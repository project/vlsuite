<?php

namespace Drupal\Tests\vlsuite\Functional;

use Drupal\Tests\vlsuite\Functional\VlsuiteAdminTestBase;

/**
 * VLSuite admin tests.
 *
 * @group vlsuite
 */
class VlsuiteAdminTest extends VlsuiteAdminTestBase {

  /**
   * {@inheritdoc}
   */
  protected $vlsuiteAdminSubPath = NULL;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'vlsuite',
    // Enable any other module with route to avoid empty links.
    'vlsuite_animations',
  ];
}
